import java.util.*;
import java.lang.Object;
public class MainClass {
	public static void main(String[] args){
		//Person people[] = new Person()[5];
	ArrayList<Person> people = new ArrayList<Person>();
	people.add(new BlueDevil("Ziqi","Pei","China","female","zp33","0863518","ECE","MS",false));;
	people.add(new BlueDevil("Qing","Lu","China","female","ql101","0125832","MEM","MENG",false));
	people.add(new BlueDevil("Yuanyuan","Yu","China","female","yy194","0746728","ECE","MENG",true));
	people.add(new BlueDevil("Lei","Chen","China","male","lc294","0766829","ECE","MS",true));
	people.add(new BlueDevil("Shalin", "Shah","India","female","sns37","0670910","ECE","Phd",true));
	people.add(new Person("Anran","Chen",18,"China","female"));
	people.add(new Person("Mike","Wilson",30,"America","male"));
	people.add(new BlueDevil("Sihao","Yao","China","female","sy167","0747715","ECE","MS",true));
	people.add(new BlueDevil("You","Lyu","China","male","yl489","0762305","ECE","MS",true));
	people.add(new BlueDevil("Zhongyu","Li","China","male","zl258","0766101","ECE","MS",true));
	people.add(new BlueDevil("Adel","Fahmy","Egypt","male","aff6","0849189","ECE","Professor",true));
	people.add(new BlueDevil("Ric","Telford","America","male","rt113","0664541","ECE","Professor",true));
	Scanner scanner = new Scanner(System.in);
	while(true) {
	System.out.println("Enter 0 to exit; Enter 1 to search person by full name; Enter 2 to search by country; Enter 3 to add a new person information");
	
	String choice = scanner.nextLine();
	//scanner.close();
	if(choice.equals("0")) {
		break;
	}
	else if (choice.equals("1")) {
		whoIs(people);
	}
	else if (choice.equals("2")) {
		searchCountry(people);
	}
	else if(choice.equals("3")) {
		addPerson(people);
	}
	else{
		System.out.println("Invalid Input! Please enter again!");
		continue;
	}
	System.out.println();
	//scanner.close();
	}
	System.out.println("System exits");
	scanner.close();
	}
	
	static void whoIs(ArrayList<Person> list) {
		System.out.print("Enter first name:");
		Scanner scanner  = new Scanner(System.in);
		String first = scanner.nextLine();
		System.out.print("Enter last name:");
		//Scanner scanner1  = new Scanner(System.in);
		String last = scanner.nextLine();
		//scanner.close();
		boolean check_for_find = false;
		for(Person p:list) {
			if(p.firstName.equalsIgnoreCase(first)&&p.lastName.equalsIgnoreCase(last)) {
				p.displayInfo();
				check_for_find = true;
				System.out.println();
			}
		}
		if(!check_for_find) {
		System.out.println("Find No Result!");}
	}
	static void addPerson(ArrayList<Person> list) {
		System.out.print("First Name:");
		Scanner scanner = new Scanner(System.in);
		String first = scanner.nextLine();
		System.out.print("Last Name:");
		String last = scanner.nextLine();
		System.out.print("Age:");
		int age = 0;
		try {
		age = scanner.nextInt();
		}
		catch(InputMismatchException e){
			System.err.println("Invalid age!");
			return;
		}
		if(age<=0||age>100) {
			System.out.println("Invalid age!");
			return;
		}
		
		Scanner scanner2 = new Scanner(System.in);
		System.out.println("Country:");
		String country = scanner2.nextLine();
		System.out.println("Gender:");
		String gender = scanner2.nextLine();
		if(!gender.equals("male")&&!gender.equals("female")) {
			System.err.println("Invalid gender!");
			scanner2.close();
			return;
		}
		System.out.println("University:");
		String university = scanner2.nextLine();
		if(university.equalsIgnoreCase("Duke")||university.equalsIgnoreCase("Duke University")) {
			System.out.println("NetID:");
			String netId = scanner2.nextLine();
			System.out.println("Unique ID:");
			String uniqueId = scanner2.nextLine();
			System.out.println("Major:");
			String major = scanner2.nextLine();
			System.out.println("Program:");
			String program = scanner2.nextLine();
			System.out.println("Work Experience:(yes or no)");
			String work_exp = scanner2.nextLine();
			boolean workExp = false;
			if(work_exp.equals("yes")) {
				workExp = true;
			}
			else if(work_exp.equals("no")) {
				workExp = false;
			}
			else {
				System.out.println("Please Enter yes or no");
				return;
			}
			list.add(new BlueDevil(first,last,country,gender,netId,uniqueId,major,program,workExp));
		}
		else {
			list.add(new Person(first,last,age,country,gender));
		}
		
		System.out.println("add successfully!");
	}
	static void searchCountry(ArrayList<Person> list) {
		System.out.print("Country:");
		Scanner scanner = new Scanner(System.in);
		String country = scanner.nextLine();
		boolean check_for_find = false;
		for(Person p:list) {
			if(p.getCountry().equalsIgnoreCase(country)) {
				p.displayInfo();
				System.out.println();
				check_for_find = true;
			}
		}
		if(!check_for_find) {
			System.out.println("Could not find result!");
		}
	}
}


