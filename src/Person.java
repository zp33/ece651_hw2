
public class Person {
	protected String firstName;
	protected String lastName;
	protected int age;
	protected String country;
	protected String gender;
	
	Person(){
		this.firstName = null;
		this.lastName = null;
		this.age = 0;
		this.country = null;
		this.gender = null;
	}
	Person(String firstName, String lastName, int age, String country, String gender){
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.country = country;
		this.gender = gender;
	}
	
	public void displayInfo(){
		String Gender = gender.equals("male")?"He":"She";
		//System.out.println(firstName +" "+lastName+" is from "+ country+ ".");
		System.out.printf("%s %s is from %s. %s is %d years old.", firstName,lastName,country,Gender,age);
		
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getCountry() {
		return country;
	}
	public int getAge() {
		return age;
	}
}
