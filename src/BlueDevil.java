
public class BlueDevil extends Person{
	private String netId;
	private String uniqueId;
	private String major;
	private String program;
	private boolean workExp;
	
	BlueDevil(String firstName, String lastName, String country, String gender, String netId,String uniqueId,String major,String program,boolean workExp){
		super.firstName = firstName;
		//super.age = age;
		super.lastName = lastName;
		super.country = country;
		super.gender = gender;
		this.netId = netId;
		this.uniqueId = uniqueId;
		this.major = major;
		this.program = program;
		this.workExp = workExp;
		
	}
	public void displayInfo(){
		//super.displayInfo();
		String Gender = gender.equals("male")?"He":"She";
		String pronouns = gender.equals("male")?"His":"Her";
		String title = program.equalsIgnoreCase("professor")?"professor":(program+" student");
		String work_exp = workExp?"":"no";
		System.out.printf("%s %s is a %s %s at Duke University.\n%s is from %s.\n%s netID is %s and unique id is %s.\nEmail is %s@duke.edu\n%s has %s work experience.\n",super.getFirstName(),super.getLastName(),major,title,Gender,country,pronouns,netId,uniqueId,netId,Gender,work_exp);
	}
}
