# ECE651_HW2

Ziqi Pei (zp33)

Please use Eclipse to compile and run my program

The program contains three classes: Person, BlueDevil and Mainclass

Person: This class is each person with attributes firstName, lastName, gender, country, age. It provides two constructors to initialize person object.
    displayInfo() -- display the information of each person
    
BlueDevil: It inherits from Person class and has attributes netId, uniqueId, major, program and work experience. It also has constructor and override parent's displayInfo() function

MainClass: Includes main method and several functions
    main() -- Some basic information about TAs and Professors are added into the list.  
            Users can enter 1 to search people by full name, 2 to filter people by country and 3 to add new person. This system always work until the user enters 0.(exit system)
        whoIs() -- ask users to enter first name and last name and then iterate through the list to find the person and show his/her information.
        addPerson() -- ask users to enter basic information of person. If the university is duke, It will ask users to add more information -- the attributes of Bluedevil
                        It then create the object and add to the array list.
                        It can also deal with some invalid input by throwing some exceptions and ask to add again.
        searchCountry() -- search people by the country enter by the user and print their information.
        
        
    